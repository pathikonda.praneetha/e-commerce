
import {Cart_Items, Special_Damaka, Deal_Of_The_Day, Based_On_Interest, Discount_Corner, Advertisement} from "./model.js";

/**
 * Adds items to the cart for the specified model
 * @param {data} product details 
 */
 function Add_cart_item(model_no,data) {
    const model = [Cart_Items, Special_Damaka, Deal_Of_The_Day, Based_On_Interest, Discount_Corner, Advertisement];
    const cart_item = new model[model_no](data);
    cart_item
    .save()
    .then(()=> {
        console.log("Item added");
                })
    .catch((err)=> console.log(err))
}


/**
 * Gets all items in the cart using the model of
 */
function Items_in_cart(model_no) {
    const model = [Cart_Items, Special_Damaka, Deal_Of_The_Day, Based_On_Interest, Discount_Corner, Advertisement];
    model[model_no].find({})
    .then((result) =>{
        console.log(result);
    })
    .catch((err)=> {
        console.log(err);
    })
}


function Get_all_products() {
    // hi

}

export {Add_cart_item, Items_in_cart, Get_all_products};