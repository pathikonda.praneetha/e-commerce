import mongoose from "mongoose";


// cart_schema, which is used for validating required details of the product
const cart_schema = mongoose.Schema (
                                     {image:{type:String, required:true}, 
                                      product_name: {type:String, required:true},
                                      price: {type:Number, required:true}}
                                    )

// special_offer_schema, which is used to validate the required details of the product
const special_damaka_schema = mongoose.Schema (
                                                {image:{type:String, required:true}, 
                                                 offer: {type:String, required:true},
                                                 sale: {type:String, required:true},
                                                 status: {type:String, required:true}}
                                              )       

// deals_of_the_day_schema,  which is used to validate the required details of the product
const deals_of_the_day_schema = mongoose.Schema (
                                                {image:{type:String, required:true}, 
                                                offer: {type:String, required:true},
                                                available: {type:String, required:true},
                                                product_name: {type:String, required:true},
                                                description: {type:String, required:true},
                                                price: {type:String, required:true}}
                                                )  

// based_on_interest_schema, which is used to validate the required details of the product
const based_on_interest_schema = mongoose.Schema(
                                                {image:{type:String, required:true}, 
                                                 description: {type:String, required:true},
                                                 rating: {type:Number, required:true},
                                                 actual_price: {type:Number, required:true},
                                                 net_price: {type:Number, required:true}}
                                                )

// discount_corner_schema, which is used to validate the required details of the product
const discount_corner_schema = mongoose.Schema  (
                                                {image:{type:String, required:true}, 
                                                product_name: {type:String, required:true},
                                                rating: {type:Number, required:true},
                                                status: {type:String, required:true}}
                                                )

// advertisement_schema 
const advertisement_schema = mongoose.Schema ({image:{type:String, required:true}})

const Cart_Items = mongoose.model("CART_ITEMS", cart_schema);
const Special_Damaka = mongoose.model("Special_Damaka", special_damaka_schema);
const Deal_Of_The_Day = mongoose.model("Deal_Of_The_Day",deals_of_the_day_schema);
const Based_On_Interest = mongoose.model("Based_On_Interest",based_on_interest_schema);
const Discount_Corner = mongoose.model("Discount_Corner", discount_corner_schema);
const Advertisement = mongoose.model("Advertisement", advertisement_schema);



export {Cart_Items, Special_Damaka, Deal_Of_The_Day, Based_On_Interest, Discount_Corner, Advertisement};