// importing express
import express from "express";
// importing body-parser
import bodyParser from "body-parser";
// importing mongoose
import mongoose from "mongoose";

// creating an instance of express
const app = express();
// returns middleware that only parses json
app.use(bodyParser.json());
// returns middleware that only parses urlencoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

import {Add_cart_item, Items_in_cart, Get_all_products} from "./product_handlers.js";


// app.get("/",(req,res)=> {
//     try{
//         Get_all_products();
//         res.sendStatus(200);
//     }
//     catch(err){
//         res.sendStatus(400);
//     }
// })


/**
 * To Get Items in Cart
 * @param {response} statuscode "200" for successfully getting items to cart, else "404"
 */
 app.get("/items/:model_no",(req,res)=>{
    const { model_no }= req.params; 
    try{
        Items_in_cart(model_no);
        res.sendStatus(200);
    }
    catch(err){
        res.sendStatus(400);
    }
})

/**
 * To Add Items in Cart
 * @param {requests} requests items data from the body
 * @param {response} statuscode "200" for successfully adding items to cart, else "400"
 */
app.post("/items/:model_no",(req,res)=>{
    const data = req.body;
    const { model_no }= req.params;
    try{
        Add_cart_item(model_no,data);
        res.sendStatus(200);
    }
    catch(err){
        res.sendStatus(400);
    }
})



mongoose.connect("mongodb+srv://mohanladdika480:hsj69gUM5IeWKKCl@cluster0.zudh5.mongodb.net/myFirstDatabase?retryWrites=true&w=majority",()=>{
    app.listen(3000,()=>{
        console.log("Port listening to 3000")
    })
});
