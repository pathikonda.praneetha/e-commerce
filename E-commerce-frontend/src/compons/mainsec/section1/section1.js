import { Component } from "react";
import {Section1Helper} from "./section1_helper";

class Section1 extends Component {
    render() {
        const {data} = this.props;
        return(
            <section className="section1">
              <Section1Helper data={data}/>
            </section>
        )
    }
}

export {Section1}