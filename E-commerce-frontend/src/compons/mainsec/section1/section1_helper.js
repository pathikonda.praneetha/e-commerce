import { Component } from "react";

class Section1Helper extends Component {
    render() {
        const {data} = this.props;
        return( data.map((data) => {
            return (
                <div
        className="add-card"
        style={{
            background:
              "linear-gradient(rgba(1,1,1,.5), rgba(1,1,1,.5))",
              backgroundImage: data.image
          }}
      >
        <section>
          <span className="title">{data.sale}</span>
          <span className="sub-title">{data.offer}</span>
          <a href="" className="btn1">
            {data.status}
          </a>
        </section>
      </div>
            )
        })
            
        )
    }
}

export {Section1Helper}