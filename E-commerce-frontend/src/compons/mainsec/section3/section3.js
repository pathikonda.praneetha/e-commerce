import { Component } from "react";
import {Section3Helper} from "./section3_helper";

class Section3 extends Component{
    render() {
      const {data} = this.props;
        return(
            <div>
                <div className="section3-header">
      <span className="title">BASED ON YOUR INTEREST</span>
      <a href="#" className="btn1 viewmore">
        View More
      </a>
      <button className="next" onclick="scrollright()">
        <i className="fa fa-angle-right" />
      </button>
      <button className="previous" onclick="scrollleft()">
        <i className="fa fa-angle-left" />
      </button>
                </div>
                <section className="section3" id="section3">
                <Section3Helper data={data}/>
                </section>
            </div>
        )
    }
}

export {Section3};