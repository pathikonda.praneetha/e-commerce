import {Component} from "react";
import {Section4Helper} from "./section4_helper";

 class Section4 extends Component {
     render() {
       const {data} = this.props;
         return(
             <div>
                 <div className="section4-header">
                    <span className="title">DISCOUNT CORNER</span>
                    <a href="#" className="btn1 viewmore">
                      View More
                    </a>
                    <button className="next" onclick="scrollright4()">
                      <i className="fa fa-angle-right" />
                    </button>
                    <button className="previous" onclick="scrollleft4()">
                      <i className="fa fa-angle-left" />
                    </button>
                 </div>
                <section className="section4" id="section4">
                  <Section4Helper data={data}/>
                </section>
             </div>
         )
     }
 }

 export {Section4};