import { Component } from "react";

class Section4Helper extends Component{
    render() {
        const {data} = this.props;
        return( data.map((data)=>{
            return(
                <div className="discount-card">
        <img src={data.image} alt="Product" />
        <a href="#" className="like-button">
          <i className="fa fa-heart-o" />
        </a>
        <span className="title">{data.product_name}</span>
        <span className="rating">
          <span className="fa fa-star checked" />
          <span className="fa fa-star checked" />
          <span className="fa fa-star checked" />
          <span className="fa fa-star" />
          <span className="fa fa-star" />
          {data.rating}
        </span>
        <span className="btn2">{data.status}</span>
      </div>
            )
        })
            
        )
    }
}

export {Section4Helper}