import { Component } from "react";

class Section6 extends Component {
    render() {
        return(
          <section className="section6">
            <table>
              <tbody>
                <tr>
                  <td>
                    <img src="http://localhost:3000/Images/Mainsec/Section6/app.png" alt="APP" />
                  </td>
                  <td>
                    <span className="title">Download INFERNO App Now</span>
                    <span className="sub-title">
                      Fast, Simple &amp; Delightful. All it takes is 15 seconds to
                      Download.
                    </span>
                    <a href="#" className="btn1">
                      <i className="fa fa-android" /> DOWNLOAD
                    </a>
                  </td>
                </tr>
              </tbody>
            </table>
          </section>
        )
    }
}

export {Section6}