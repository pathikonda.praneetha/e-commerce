import { Component } from "react";
import {Section1} from "./section1/section1";
import {Section2} from "./section2/section2";
import {Section3} from "./section3/section3";
import {Section4} from "./section4/section4";
import {Section6} from "./section6/section6";
import {Section5} from "./section5/section5";

class Mainsec extends Component {
    render() {
        const {data} = this.props;
        return(
            <div>
                <Section1 data={data[0]}/>
                <Section2 data={data[1]}/>
                <Section3 data={data[2]}/>
                <Section4 data={data[3]}/>
                <Section5 data={data[4]}/>
                <Section6 data={data[5]}/>
            </div>
        )
    }
}

export {Mainsec};