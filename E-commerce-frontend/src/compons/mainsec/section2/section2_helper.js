
import {Component} from "react";

class Section2helper extends Component {
    render() {
        const {data} = this.props;
    return (data.map((data)=>{
        return(
            <div className="deal-card">
        <img src= {data.image} alt="Deals" />
        <span className="discount">
          <i className="fa fa-percentage" /> {data.offer}
        </span>
        <span className={data.tag_class}>
          <i className={data.check_class} /> {data.available}
        </span>
        <span className="title"> {data.product_name} </span>
        <span className="sub-title">
          {data.description}
        </span>
        <a href="#" className="btn1 soldout">
        {data.price}
        </a>
      </div>
        )
    }))
    
}
}

export {Section2helper};