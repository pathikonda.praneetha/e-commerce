import { Component } from "react";
import {Section2helper} from "./section2_helper";

class Section2 extends Component {
    render() {
      
      const {data}=this.props;
        return(
            <div>
    <h1 className="title section2-header">
      DEALS OF THE DAY{" "}
      <span id="timer">
        <i className="fa fa-clock-o" />
      </span>
    </h1>
    <section className="section2">
         <Section2helper data={data[0]}/>
      <div className="deal-card advertise-card" style={{backgroundImage: data[1].image}}>
      </div> 
    </section>
    </div>
        )
    }
}

export {Section2}