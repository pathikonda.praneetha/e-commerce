import {Component} from "react";
import {Menubar} from "./menubar/menubar";
import {Navbar} from "./navbar/navbar"

class Header extends Component {
  render() {
    const {data} = this.props;
    return(
      <header>
        <Menubar data={data}/>
        <Navbar/>
      </header>)
}
}

export {Header}