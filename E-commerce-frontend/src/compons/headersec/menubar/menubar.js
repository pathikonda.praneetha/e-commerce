import { Component } from "react";
import {MainbarHelper} from "./mainbar_helper";

class Menubar extends Component{
    render() {
        const {data} = this.props;
        return(
            <div className="menu">
          <table>
            <tbody>
              <tr>
                <td>
                  <img src="http://localhost:3000/Images/Header/logo.png" alt="INFERNO" className="logo" />
                  <p> INFERNO </p>
                </td>
                <td>
                  <input
                    type="text"
                    className="search"
                    name="search"
                    placeholder="Search"
                  />
                  <button type="submit">
                    <i className="fa fa-search" />
                  </button>
                </td>
                <td>
                  <span>
                    <div className="navbar">
                      <div className="dropdown">
                        <button className="dropbtn">
                          Account
                          <i className="fa fa-caret-down" />
                        </button>
                        <div className="dropdown-content">
                          <a href="#">Profile</a>
                          <a href="#">Orders</a>
                          <a href="#">Wishlist</a>
                          <a href="#">LogIn/LogOut</a>
                        </div>
                      </div>
                      <div className="dropdown">
                        <button className="dropbtn cart">
                          <i className="fa fa-shopping-cart" />{" "}
                          <span className="value">2</span>
                        </button>
                        <div className="dropdown-content cart-content">
                          <a>
                            <MainbarHelper data={data}/>
                            <button type="submit" className="btn1">
                              CheckOut
                            </button>
                          </a>
                        </div>
                      </div>
                    </div>
                  </span>
                </td>
              </tr>
            </tbody>
          </table>
            </div>
        )
    }
}

export {Menubar};