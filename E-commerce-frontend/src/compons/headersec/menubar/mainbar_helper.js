import { Component } from "react";

class MainbarHelper extends Component {
    render() {
        const {data} = this.props;
        return( data.map((data)=>{
            return(
                <ul>
                              <li>
                                <img
                                  src={data.image}
                                  alt="Item"
                                />
                              </li>
                              <li>
                                <span className="title">{data.product_name}</span>
                                <span className="sub-title">
                                  <i className="fa fa-rupee" />
                                  {data.price}<span></span>
                                </span>
                              </li>
                              <li>
                                <i className="fa fa-trash" />
                              </li>
                            </ul>
            )
        })

        )
    }
}

export {MainbarHelper};