import { Component } from "react";

class Navbar extends Component{
    render() {
        return(
            <div className="topnav" id="myTopnav">
            <div className="dropdown">
              <button className="dropbtn">
                Electronics
                <i className="fa fa-caret-down" />
              </button>
              <div className="dropdown-content mega-dropdown">
                <div className="row  column1">
                  <div className="column">
                    <h3>Mobiles</h3>
                    <a href="#">Mi</a>
                    <a href="#">Samsung</a>
                    <a href="#">RealMe</a>
                  </div>
                  <div className="column">
                    <h3>Mobile Accessories</h3>
                    <a href="#">Power Bank</a>
                    <a href="#">OTG</a>
                    <a href="#">Covers</a>
                  </div>
                  <div className="column">
                    <h3>Speakers</h3>
                    <a href="#">Home Theater</a>
                    <a href="#">Portable</a>
                    <a href="#">Sound Bars</a>
                  </div>
                  <div className="column">
                    <h3>Featured</h3>
                    <a href="#">Apple Store</a>
                    <a href="#">Mcrosoft Store</a>
                    <a href="#">Inferno Store</a>
                  </div>
                </div>
              </div>
            </div>
            <div className="dropdown">
              <button className="dropbtn">
                Men &amp; Women
                <i className="fa fa-caret-down" />
              </button>
              <div className="dropdown-content mega-dropdown">
                <div className="row">
                  <div className="column">
                    <h3>Clothing</h3>
                    <a href="#">Men Jeans</a>
                    <a href="#">Women Jeggings</a>
                    <a href="#">Trousers</a>
                  </div>
                  <div className="column">
                    <h3>Grooming</h3>
                    <a href="#">Perfumes</a>
                    <a href="#">Deodrants</a>
                    <a href="#">Hair Groom</a>
                  </div>
                  <div className="column">
                    <h3>Footware</h3>
                    <a href="#">Casual</a>
                    <a href="#">Boots</a>
                    <a href="#">Slippers</a>
                  </div>
                </div>
              </div>
            </div>
            <div className="dropdown">
              <button className="dropbtn">
                Home &amp; Furniture
                <i className="fa fa-caret-down" />
              </button>
              <div className="dropdown-content mega-dropdown">
                <div className="row">
                  <div className="column">
                    <h3>Kitchen</h3>
                    <a href="#">Cook Ware</a>
                    <a href="#">Electric Corner</a>
                    <a href="#">Infrastructure</a>
                  </div>
                  <div className="column">
                    <h3>Home Improvements</h3>
                    <a href="#">Cleaners</a>
                    <a href="#">Perfumes</a>
                    <a href="#">Sanitizer</a>
                  </div>
                  <div className="column">
                    <h3>Living Room</h3>
                    <a href="#">Lightning</a>
                    <a href="#">Dining Table</a>
                    <a href="#">Jugs</a>
                  </div>
                </div>
              </div>
            </div>
            <div className="dropdown">
              <button className="dropbtn">
                Sport &amp; Fitness
                <i className="fa fa-caret-down" />
              </button>
              <div className="dropdown-content mega-dropdown">
                <div className="row">
                  <div className="column">
                    <h3>Sport</h3>
                    <a href="#">Cricket</a>
                    <a href="#">Football</a>
                    <a href="#">Volley Ball</a>
                  </div>
                  <div className="column">
                    <h3>Fitness</h3>
                    <a href="#">Dumbells</a>
                    <a href="#">Mini-Set</a>
                    <a href="#">Powders</a>
                  </div>
                  <div className="column">
                    <h3>Featured</h3>
                    <a href="#">Suppliments</a>
                    <a href="#">Gadgets</a>
                    <a href="#">Kits</a>
                  </div>
                </div>
              </div>
            </div>
            <div className="dropdown">
              <button className="dropbtn">
                Kids Store
                <i className="fa fa-caret-down" />
              </button>
              <div className="dropdown-content mega-dropdown">
                <div className="row">
                  <div className="column">
                    <h3>Toys</h3>
                    <a href="#">Dolls</a>
                    <a href="#">E-Gadgets</a>
                    <a href="#">Tabs</a>
                  </div>
                  <div className="column">
                    <h3>Books</h3>
                    <a href="#">Story Books</a>
                    <a href="#">Educational</a>
                    <a href="#">Guidlines</a>
                  </div>
                  <div className="column">
                    <h3>Care</h3>
                    <a href="#">Body Care</a>
                    <a href="#">Hair Care</a>
                    <a href="#">Growth</a>
                  </div>
                </div>
              </div>
            </div>
            <div className="dropdown">
              <button className="dropbtn">
                More
                <i className="fa fa-caret-down" />
              </button>
              <div className="dropdown-content">
                <a href="#">Notifications</a>
                <a href="#">Customer Care</a>
                <a href="#">Download App</a>
              </div>
            </div>
            <a href="#about">Coupons</a>
            <div className="dropdown responsive">
              <button className="dropbtn">
                Account
                <i className="fa fa-caret-down" />
              </button>
              <div className="dropdown-content">
                <a href="#">Profile</a>
                <a href="#">Orders</a>
                <a href="#">Wishlist</a>
                <a href="#">Login</a>
              </div>
            </div>



            {/* <div className="dropdown responsive">
              <button className="dropbtn">
                <i className="fa fa-shopping-cart" />
                <span className="value">2</span>
              </button>
              <div className="dropdown-content">
                <a>
                  <ul>
                    <li>
                      <img src="https://i.ibb.co/RpnQq12/01.webp" alt="Item" />
                    </li>
                    <li>
                      <span className="title">Samsung Galaxy POP</span>
                      <span className="sub-title">
                        <i className="fa fa-rupee" />
                        5999<span></span>
                      </span>
                    </li>
                    <li>
                      <i className="fa fa-trash" />
                    </li>
                  </ul>
                  <ul>
                    <li>
                      <img src="https://i.ibb.co/RpnQq12/01.webp" alt="Item" />
                    </li>
                    <li>
                      <span className="title">Samsung Galaxy POP</span>
                      <span className="sub-title">
                        <i className="fa fa-rupee" />
                        5999<span></span>
                      </span>
                    </li>
                    <li>
                      <i className="fa fa-trash" />
                    </li>
                  </ul>
                  <button type="submit" className="btn1">
                    CheckOut
                  </button>
                </a>
              </div>
            </div>
            <a
              href="javascript:void(0);"
              style={{ fontSize: 15 }}
              className="icon"
              onclick="myFunction()"
            >
            </a> */}

            
          </div>
        )
    }
}

export {Navbar};