
const Header_data = [
                    {
                        image: "http://localhost:3000/Images/Header/pic1.webp",
                        product_name: "Samsung Galaxy POP",
                        price: "5999"
                    },
                    {
                        image: "http://localhost:3000/Images/Header/pic1.webp",
                        product_name: "Samsung Galaxy POP",
                        price: "5999"
                    }
                    ]

const Section1_data = [{image:"url(http://localhost:3000/Images/Mainsec/Section1/card1.jpg)",
                        sale:"New Year Bonanza",
                        offer:"Upto 25% Off On Washing Machines",
                        status: "Grab Now"
                        },
                        {
                            image:"url(http://localhost:3000/Images/Mainsec/Section1/card2.jpg)",
                            sale:"New Year Bonanza",
                            offer:"Upto 30% Off On Foot Wear",
                            status: "Grab Now"
                        },
                        {
                            image:"url(http://localhost:3000/Images/Mainsec/Section1/card3.jpg)",
                            sale:"New Year Bonanza",
                            offer:"Upto 15% Off On Mobiles",
                            status: "Grab Now"
                        }]

const Section2_data =   [
                            [{
                                image:"http://localhost:3000/Images/Mainsec/Section2/card3.jpg",
                                offer: "20% OFF",
                                available: "Sold Out",
                                product_name: "Samsung Galaxy POP",
                                description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                                price: "Sold Out",
                                check_class: "fa fa-times",
                                tag_class: "tag-2"
                            },
                            {
                                image:"http://localhost:3000/Images/Mainsec/Section2/card3.jpg",
                                offer: "20% OFF",
                                available: "Available",
                                product_name: "Samsung Galaxy POP",
                                description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                                price: "5999",
                                check_class: "fa fa-check-circle-o",
                                tag_class: "tag-1"
                            },
                            {
                                image:"http://localhost:3000/Images/Mainsec/Section2/card3.jpg",
                                offer: "20% OFF",
                                available: "Available",
                                product_name: "Samsung Galaxy POP",
                                description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                                price: "5999",
                                check_class: "fa fa-check-circle-o",
                                tag_class: "tag-1"
                            },
                            {
                                image:"http://localhost:3000/Images/Mainsec/Section2/card3.jpg",
                                offer: "20% OFF",
                                available: "Available",
                                product_name: "Samsung Galaxy POP",
                                description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                                price: "5999",
                                check_class: "fa fa-check-circle-o",
                                tag_class: "tag-1"
                            },
                            {
                                image:"http://localhost:3000/Images/Mainsec/Section2/card3.jpg",
                                offer: "20% OFF",
                                available: "Available",
                                product_name: "Samsung Galaxy POP",
                                description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                                price: "6999",
                                check_class: "fa fa-check-circle-o",
                                tag_class: "tag-1"
                            },
                            {
                                image:"http://localhost:3000/Images/Mainsec/Section2/card3.jpg",
                                offer: "20% OFF",
                                available: "Available",
                                product_name: "Samsung Galaxy POP",
                                description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                                price: "5999",
                                check_class: "fa fa-check-circle-o",
                                tag_class: "tag-1"
                            }],
                            {
                                image: "url(http://localhost:3000/Images/Mainsec/Section2/add-card.png)"
                            }
                        ]

const Section3_data = [
                        {image: "http://localhost:3000/Images/Mainsec/Section3/01.webp",
                         description:"Lenovo-na thin laptop with 4gb ram 500gb hard disk",
                         rating:"1,030",
                         actual_price:"10200",
                         net_price:"9999"
                        },
                        {image: "http://localhost:3000/Images/Mainsec/Section3/01.webp",
                         description:"Lenovo-na thin laptop with 4gb ram 500gb hard disk",
                         rating:"1,030",
                         actual_price:"10200",
                         net_price:"9999"
                        },
                        {image: "http://localhost:3000/Images/Mainsec/Section3/01.webp",
                         description:"Lenovo-na thin laptop with 4gb ram 500gb hard disk",
                         rating:"1,030",
                         actual_price:"10200",
                         net_price:"9999"
                        },
                        {image: "http://localhost:3000/Images/Mainsec/Section3/01.webp",
                         description:"Lenovo-na thin laptop with 4gb ram 500gb hard disk",
                         rating:"1,030",
                         actual_price:"10200",
                         net_price:"9999"
                        },
                        {image: "http://localhost:3000/Images/Mainsec/Section3/01.webp",
                         description:"Lenovo-na thin laptop with 4gb ram 500gb hard disk",
                         rating:"1030",
                         actual_price:"10200",
                         net_price:"9999"
                        },
                        {image: "http://localhost:3000/Images/Mainsec/Section3/01.webp",
                         description:"Lenovo-na thin laptop with 4gb ram 500gb hard disk",
                         rating:"1030",
                         actual_price:"10200",
                         net_price:"9999"
                        },
                        {image: "http://localhost:3000/Images/Mainsec/Section3/01.webp",
                         description:"Lenovo-na thin laptop with 4gb ram 500gb hard disk",
                         rating:"1030",
                         actual_price:"10200",
                         net_price:"9999"
                        }
                      ]
                        
const Section4_data = [
                        {
                            image:"http://localhost:3000/Images/Mainsec/Section4/02.webp",
                            product_name:"Chimney",
                            rating: "930",
                            status: "Fast Sold"
                        },
                        {
                            image:"http://localhost:3000/Images/Mainsec/Section4/05.webp",
                            product_name:"Casino Men's Classic",
                            rating: "2400",
                            status: "Grab Now"
                        },
                        {
                            image:"http://localhost:3000/Images/Mainsec/Section4/03.webp",
                            product_name:"Mixer Juice Grinder",
                            rating: "230",
                            status: "30% OFF"
                        },
                        {
                            image:"http://localhost:3000/Images/Mainsec/Section4/05.webp",
                            product_name:"Casino Men's Classic",
                            rating: "2400",
                            status: "Grab Now"
                        },
                        {
                            image:"http://localhost:3000/Images/Mainsec/Section4/02.webp",
                            product_name:"Chimney",
                            rating: "930",
                            status: "Fast Sold"
                        },
                        {
                            image:"http://localhost:3000/Images/Mainsec/Section4/01.webp",
                            product_name:"Lenovo-na thin laptop with 4gb ram 500gb hard disk",
                            rating: "1030",
                            status: "New Eve Deal"
                        }
                      ]

const Section5_data = {image:"http://localhost:3000/Images/Mainsec/Section5/1.png"}

                      
const Mainsec_data = [Section1_data, Section2_data, Section3_data, Section4_data, Section5_data]




export {Header_data,Mainsec_data}