import logo from './logo.svg';
import './App.css';
import {Footer} from "./compons/footersec/footers";
import {Header} from "./compons/headersec/headers";
import {Mainsec} from "./compons/mainsec/mainsec";
import {Header_data,Mainsec_data} from "./constant"

function App() {
  return (
    <div>
    <Header data={Header_data}/>
    <Mainsec data={Mainsec_data}/>
    <Footer/>
    </div>
  );
}


export default App;
